#! /usr/bin/env python

from guardian import GuardState
import cdsutils as cdu
import gpstime

request = 'HIGH_ARM_POWER'
nominal = 'HIGH_ARM_POWER'
import lscparams as lscparams
from timeout_utils import call_with_timeout

# we need to set these thresholds so that we 14e3/only go to check shutter if and only if the shutter should have been triggered
arm_power_lower_thresh = 22e3 #at 2 Watts single arm is about 10 counts
arm_power_upper_thresh = 25e3
#calibration of these PDs is in LHO alog 15430
#3.6 ppm ETM transmission, 2.5% of the light leaving arm gets to PDs, 2621440 counts per Watt on the QPD, whitening gain of 18 dB is a gain of 7.9 (for LHO the guardian compensates for whitening gain changes with digital gain changes so it is correct to always compensate for 18 dB of whitening gain, probably not the same for LLO)
watts_circ_per_count=1/(2621440*0.025*7.9*3.6e-6)  #circulating power per count at output of ASC_X_TR_SUM
x_tr_chan='ASC-X_TR_B_NSUM_OUTPUT'
y_tr_chan='ASC-Y_TR_B_NSUM_OUTPUT'

gs13_chan = 'H1:ISI-HAM6_BLND_GS13Z_IN1_DQ'
gs13_threshold = 3e4



class INIT(GuardState):
    def main(self):
        circ_power_x=ezca[x_tr_chan]*watts_circ_per_count
        circ_power_y=ezca[y_tr_chan]*watts_circ_per_count
        # Do some arm power checks to determine where to go
        if circ_power_x >=arm_power_upper_thresh and circ_power_y >=arm_power_upper_thresh:
            return 'HIGH_ARM_POWER'
        else:
            return 'LOW_ARM_POWER'


class LOW_ARM_POWER(GuardState):
    index = 1
    def run(self):
        circ_power_x=ezca[x_tr_chan]*watts_circ_per_count
        circ_power_y=ezca[y_tr_chan]*watts_circ_per_count
        if circ_power_x >=arm_power_upper_thresh and circ_power_y >=arm_power_upper_thresh:
            log('power went above threshold')
            return 'HIGH_ARM_POWER'

class HIGH_ARM_POWER(GuardState):
    index = 10
    def run(self):
        circ_power_x=ezca[x_tr_chan]*watts_circ_per_count
        circ_power_y=ezca[y_tr_chan]*watts_circ_per_count
        if circ_power_x < arm_power_lower_thresh or circ_power_y < arm_power_lower_thresh:
            log('power went below threshold')
            return 'CHECK_SHUTTER'
        else:
            return True


class CHECK_SHUTTER(GuardState):
    """Run a shutter check via the HAM6 GS13 Z dof looking for a kick. 

    This state makes the assumption that when it enters this state,
    the fast shutter should have fired just before this. This should
    be OK for autonomous operation since the node would always check on
    lock loss, but for node restarts or other issues where this state is
    run not after a lock loss, it would fail. An operator would need to 
    intervene to reset the node, but presumably in a situation like this,
    there is an operator.
    """
    index = 20
    def main(self):
        self.state_start = int(gpstime.gpsnow())
        # Give some buffer time
        self.timer['datawait'] = 5
        log('Waiting before grabbing data')

    def run(self):
        if self.timer['datawait']:
            # After waiting for a data time buffer, get 10sec before entering the state
            # and 2 sec after, to make sure that we catch the shutter firing.
            log('Getting data...')
            gs13data = call_with_timeout(cdu.getdata, gs13_chan, 12, self.state_start-10)
            if gs13data:
                if max(abs(gs13data.data)) > gs13_threshold:
                    log(f'GS13 saw a kick.  Peak GS13 signal = {max(abs(gs13data.data))}')
                    return True
                else:
                    log(f'No kick, but should have. Peak GS13 signal = {max(abs(gs13data.data))}')
                    return 'SHUTTER_FAIL'
            else:
                log('No data found in data grab')
                # This should allow it to run again and try for data again
                return False
        else:
            return False


class SHUTTER_FAIL(GuardState):
    index=30
    def run(self):
        notify('Manually check shutter, then INIT to clear')
        
        


edges = [
    ('LOW_ARM_POWER','HIGH_ARM_POWER'),
    ('CHECK_SHUTTER', 'LOW_ARM_POWER')
]
